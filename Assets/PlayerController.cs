﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] Transform Camara;
    [SerializeField] float mouseSensitivity = 1;

    float verticalLookRotation;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (Timer.GameEnded)
            return;

        // Rotacion de Camara
        transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X")* mouseSensitivity);
        verticalLookRotation -= Input.GetAxisRaw("Mouse Y") * mouseSensitivity;
        verticalLookRotation = Mathf.Clamp(verticalLookRotation, -90f, 90f);
        Camara.localEulerAngles = new Vector3(verticalLookRotation, 0, 0);
    }
}
