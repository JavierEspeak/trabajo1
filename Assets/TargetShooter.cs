﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetShooter : MonoBehaviour
{
    public static Action OnTargetMissed;

    [SerializeField] Camera cam;

    public GameObject Sonidodisparo;


    void Update()
    {
        if (Timer.GameEnded)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(Sonidodisparo);
            // Raycast nos ayudara a identificar el mouse dentro del Juego y reconocera el golpe en el tarjet con el collider.
            Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f));
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                Target1 target1 = hit.collider.gameObject.GetComponent<Target1>();

                if (target1 != null)
                {
                    target1.Hit();
                }
                else
                {
                    OnTargetMissed?.Invoke();
                }
                
            }
            else
            {
                OnTargetMissed?.Invoke();
            }
        }
    }
}
