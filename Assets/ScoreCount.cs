﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreCount : MonoBehaviour
{
	[SerializeField] TMP_Text text;
	public static int Score { get; private set; }

	void OnEnable()
	{
		Target1.OnTargetHit += OnTargetHit;
	}

	void OnDisable()
	{
		Target1.OnTargetHit -= OnTargetHit;
	}

	void OnTargetHit()
	{
		Score++;
		text.text = $"Puntaje: {Score}";
	}
}
