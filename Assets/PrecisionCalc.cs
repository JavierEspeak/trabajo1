﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PrecisionCalc : MonoBehaviour
{
	[SerializeField] TMP_Text accuracyText;

	void OnEnable()
	{
		Timer.OnGameEnded += CalculateAccuracy;
	}

	void OnDisable()
	{
		Timer.OnGameEnded -= CalculateAccuracy;
	}

	void CalculateAccuracy()
	{
		float accuracy = (float)ScoreCount.Score / (float)(ScoreCount.Score + MissCount.Misses);
		accuracy *= 100f;
		accuracyText.text = $"Precisión: {accuracy.ToString("0")}%";
	}
}
