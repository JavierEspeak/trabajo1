**Descripción de proyecto**

Aim Matrix Dimension es un videojuego FPS (First Person Shooter o disparos en primera persona) 
creado por Javier Garces, Sergio Veloso y Juan Blas.

El juego consiste en personificar a un soldado espacial encerrado en la matrix en una estación en Dickerson, uno de los planetas del sistema KA20. 
En un segundo, diversos enemigos esfericos que buscan apoderarse de la matrix aparecen frente a los ojos del protagonista. 
Eres el único ser humano superviviente en la estación y tu misión es destruir todo enemigo esferico que amenasa la estación.

**Instrucciones**

En Aim Matrix Dimension las reglas son basicas.
El jugador consta con 60 Segundos, donde en este tiempo debera ponerse a prueba en Punteria, Velocidad y Reaccion. Como objetivo principal se busca lograr la puntuacion mas alta 
junto con lograr un buen porcentaje de precisión derribando los objetivos. 
Puedes volver a jugar las veces que creas necesaria para superarte y mostrarle a tus contrincantes lo fuerte que eres.

